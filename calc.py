"""A bb trajectory calculator taking into account the forces:
    - gravity, according to standard gravity equation
    - drag, according to the drag equation
    - Magnus force
    - viscous torque
Written by David Szeto.

Note that the most accurate formulas I could find on the internet were applied
directly. For example, the drag equation doesn't account for the fact that spin
apparently increases drag, according to the empirical results in this paper:
http://www.physics.usyd.edu.au/~cross/TRAJECTORIES/42.%20Ball%20Trajectories.pdf

Also, viscous torque likely increases as speed increases, because the surface
of the ball experiences more total air pressure, but the formula I used doesn't
account for this factor.

The formulas I used assume that a BB is a perfectly smooth solid sphere. IRL,
there would be some degree of roughness to the surface, which would likely
increase drag, Magnus force, and viscous torque.
    
Other factors not simulated or accounted for, which would be present IRL:
    - Joule creep: the phenomenon where heavier BBs tend to leave the same gun
    with more energy
    - the phenomenon where increasing hop-up decreases FPS (though to some
    degree this phenomenon and Joule creep tend to cancel each other out, since
    heavier BBs require far more hop)
    - the fact that heavy BBs (eg. .4g or heavier) cannot be successfully
    lifted by many hop-ups
    - air bubbles inside BBs that weigh less than .25g, which would increase the
    BB's moment of inertia. This would cause the viscous torque to have less of
    an effect on the BB's angular momentum IRL. Because of this phenomenon,
    this calculator should only be used for .25g BBs or heavier
    - variance in FPS, muzzle direction, BB weight
Written by David Szeto
"""

import argparse
import math
from matplotlib import pyplot as plt
import numpy as np
import time

import const


def main():
    plt.ion()  # prevent matplotlib blocking
    parser = argparse.ArgumentParser(description='Simulates BB trajectory.')
    parser.add_argument('--optimize', action='store_true',
                        help='if given, will attempt to tune hop-up and muzzle'
                        'angle for flattest trajectory over longest distance')
    args = parser.parse_args()

    if args.optimize:
        tune()
    else:
        plt.show()
        gun_params = GunParams()
        trajectory = fire_and_track(gun_params)
        plt.show()
        input('press enter to exit:')


class GunParams():
    """A struct of a set of tunable parameters for guns. Draws from const, but
    tunable. All attributes are public.
    """

    def __init__(self, muzzle_speed_equivalent_fps_init=None,
                 muzzle_angular_speed=None, muzzle_angle=None):
        if muzzle_speed_equivalent_fps_init is None:
            muzzle_speed_equivalent_fps_init = \
                const.MUZZLE_SPEED_EQUIVALENT_FPS_INIT
        if muzzle_angular_speed is None:
            muzzle_angular_speed = const.MUZZLE_ANGULAR_SPEED
        if muzzle_angle is None:
            muzzle_angle = const.MUZZLE_ANGLE
        self.muzzle_speed_equivalent_fps_init = \
            muzzle_speed_equivalent_fps_init
        self.muzzle_angular_speed = muzzle_angular_speed
        self.muzzle_angle = muzzle_angle

    def __repr__(self):
        return ('fps .2g equivalent: {0:.0f}; angular_speed: {1:.6f}; '
                'muzzle_angle: {2:.4f}'.format(
                    self.muzzle_speed_equivalent_fps_init,
                    self.muzzle_angular_speed,
                    self.muzzle_angle,))


class Trajectory():
    """A struct denoting the following statistics of a BB trajectory:
        -np.ndarray vector denoting the x positions of the BB over time
        -np.ndarray vector denoting the y positions of the BB over time
        -distance travelled in meters (float)
        -bb travel time in seconds (float)
        -maximum height of the BB in meters (float)
        -minimum height of the BB in meters (float)
    """
    def __init__(self, xs, ys, distance_travelled, simulation_time, max_height,
                 min_height):
        """See class docstring"""
        self.xs = xs
        self.ys = ys
        self.distance_travelled = distance_travelled
        self.simulation_time = simulation_time
        self.max_height = max_height
        self.min_height = min_height


def tune():
    """Repeatedly fires BBs and tracks their trajectory in order to come up
    with optimal muzzle angle (TODO) and muzzle angular velocity"""
    angular_speeds = np.arange(const.TUNE_ANGULAR_SPEED_RANGE[0],
                               const.TUNE_ANGULAR_SPEED_RANGE[1],
                               const.TUNE_ANGULAR_SPEED_STRIDE)
    angles = np.arange(const.TUNE_MUZZLE_ANGLES[0],
                       const.TUNE_MUZZLE_ANGLES[1],
                       const.TUNE_MUZZLE_ANGLE_STRIDE)
    trajectories = []
    gun_paramses = []
    # Run the simulations
    for angle in angles:
        for angular_speed in angular_speeds:
            gun_params = GunParams(muzzle_angular_speed=angular_speed,
                                   muzzle_angle=angle)
            trajectory = fire_and_track(gun_params)
            if trajectory.max_height <= const.MAX_HEIGHT:
                print('trajectory above accepted-----------------')
            else:
                print('trajectory above rejected-----------------')
                break
            gun_paramses.append(gun_params)
            trajectories.append(trajectory)

    # Determine which gun_params setting had the longest trajectory
    longest_distance = -1
    best_gun_params, best_trajectory = None, None
    for gun_params, trajectory in zip(gun_paramses, trajectories):
        distance = trajectory.distance_travelled
        if distance > longest_distance:
            longest_distance = distance
            best_gun_params = gun_params
            best_trajectory = trajectory

    print('Muzzle speed was fixed at an FPS equivalent of {0:.1f} with .2g BBs'
          .format(const.MUZZLE_SPEED_EQUIVALENT_FPS_INIT))
    print('BB weight was fixed at {0:.3f}g'.format(const.BB_MASS_G))
    print('The BB was allowed to rise by a max of {0:.3f}m and fall by '
          '{1:.3f}m'.format(const.MAX_HEIGHT, const.MIN_HEIGHT))
    print('The muzzle angle and angular velocity were varied')
    print('longest distance: {0:.4f}'.format(longest_distance))
    print('best gun params:', best_gun_params)
    plt.show()
    input('press enter to exit:')


def fire_and_track(gun_params):
    """Fires a BB and tracks its position over time until it hits the ground.
    Prints statistics about the current settings of the BB and gun, displays
    a plot of the trajectory in a non-blocking fashion.

    Args:
        gun_params: a GunParams instance dictating the settings of the gun
        currently being fired

    Returns:
        an instance of Trajectory denoting the BB's trajectory
    """
    t0 = time.time()
    state = fire(gun_params)
    x, y, *_ = state[0]
    xs, ys = [], []
    while y > -const.MIN_HEIGHT and x < const.MAX_DISTANCE:
        state = process_step(*state)
        x, y, *_ = state[0]
        xs.append(state[0][0])
        ys.append(y)
        simulation_time = state[-1]
        # print('position: {0}; velocity: {1}; angular velocity: {2}; time:'
        #       '{3}'.format(*state))
    xs, ys = np.asarray(xs), np.asarray(ys)
    num_steps = len(xs)
    assert xs.shape == ys.shape == (num_steps,)
    distance_travelled = xs[-1]
    max_height = max(ys)
    min_height = min(ys)
    print('bb travelled {0:.2f} meters'.format(distance_travelled))
    print('bb spent {0:.4f} seconds in the air'.format(simulation_time))
    print('maximum bb height was {0:.4f} meters above muzzle'.
          format(max_height))
    print('minimum bb height was {0:.4f} meters below muzzle'.
          format(-min_height))
    t1 = time.time()
    print('simulation took {0:.4f} seconds'.format(t1 - t0))
    print('simulation involved {0} steps'.format(num_steps))
    plt.plot(xs, ys)
    plt.draw()
    plt.pause(0.001)
    result = Trajectory(xs, ys, distance_travelled, simulation_time,
                        max_height, min_height)
    return result


def fire(gun_params):
    """Fires a BB using the constants in const.py.

    The major component of the BB's velocity will be in the x direction.
    Direction y will stand for vertical displacement from the muzzle. Direction
    z is a dummy direction pointing out of the screen; it is there solely to
    make the cross-product between velocity and angular velocity convert to
    Magnus force.

    Args:
        gun_params: an instance of GunParams denoting the params of the gun
        we'll be using to fire the BB

    Returns: tuple of
        - initial position as a 3-vector ndarray with value (0., 0., 0.)
        - initial velocity as a 3-vector ndarray
        - initial angular velocity as a 3-vector ndarray
        - time elapsed, as a float, with value 0.
    """
    print('bb weight is {0:.3f} g'.format(const.BB_MASS * 1000))
    print('firing bb at energy-equivalent of {0} FPS with .2g BBs'.format(
        gun_params.muzzle_speed_equivalent_fps_init))
    print('initial muzzle elevation angle is {0:.3f} radians'.format(
        gun_params.muzzle_angle))
    print('initial angular speed of BB is {0:.4f} radians per second'.format(
        gun_params.muzzle_angular_speed))
    position = np.asarray([0., 0., 0.])
    velocity = init_velocity(gun_params)
    angular_velocity = (gun_params.muzzle_angular_speed *
                        np.asarray([0., 0., 1.]))
    time = 0.
    return position, velocity, angular_velocity, time


def init_velocity(gun_params):
    """Given the settings from gun_params, calculates the amount of muzzle
    energy a .2g BB would have at that speed, and then returns an appropriate
    velocity vector that a BB would have, given that its mass was
    const.BB_MASS.

    Args:
        gun_params: an instance of GunParams

    Returns:
        initial velocity vector of the BB as a np.ndarray 3-vector
    """
    # speed in m / s
    equivalent_speed = (gun_params.muzzle_speed_equivalent_fps_init *
                        const.FPS_TO_MPS)
    energy = 0.5 * 0.0002 * equivalent_speed ** 2
    print('initial energy in Joules:', energy)

    actual_speed = (energy / const.BB_MASS) ** 0.5
    print('initial speed in m/s:', actual_speed)
    velocity = actual_speed * np.asarray([
        math.cos(gun_params.muzzle_angle),
        math.sin(gun_params.muzzle_angle),
        0.])
    return velocity


def process_step(position, velocity, angular_velocity, time):
    """Updates (in order):
        - position based on velocity,
        - velocity based on physics laws
        - angular velocity based on physics laws
        - time based on const.TIME_EPSILON

    Args:
        - position: np.ndarray 3-vector denoting x, y, z position of BB
        - velocity: np.ndarray 3-vector denoting dx, dy, dz velocity of BB
        - angular_velocity: np.ndarray 3-vector denoting dx, dy, dz angular
        velocity of BB
        - time: the current time elapsed in s (float)

    Returns: tuple of
        - new position
        - new velocity
        - new angular velocity
        - new time
    """
    position += velocity * const.TIME_EPSILON
    velocity, angular_velocity = process_forces(velocity, angular_velocity)
    time += const.TIME_EPSILON
    return position, velocity, angular_velocity, time


def process_forces(velocity, angular_velocity):
    """Updates velocity according to (all simultaneously):
        -gravity: in vertical direction, a_g = const.STANDARD_GRAVITY
        -drag: in opposite direction of velocity:
        f_d = const.BB_DRAG_COEFFICIENT * 0.5 * const.AIR_DENSITY *
        norm(velocity) ** 2 * const.BB_AREA
        -magnus force: https://www.grc.nasa.gov/www/k-12/airplane/beach.html
        -viscous torque: t = -8 * math.pi * const.BB_RADIUS ** 3 *
        const.AIR_VISCOSITY * angular_velocity

    Args:
        - velocity: np.ndarray 3-vector denoting dx, dy, dz velocity of BB
        - angular_velocity: np.ndarray 3-vector denoting dx, dy, dz angular
        velocity of BB

    Returns: tuple of
        - new velocity
        - new angular velocity
    """
    speed = np.linalg.norm(velocity)
    angular_speed = np.linalg.norm(angular_velocity)

    # change in velocity in meters per second ** 2
    delta_velocity = np.asarray([0., 0., 0.])

    # Gravity
    delta_velocity += const.STANDARD_GRAVITY * np.asarray([0., -1., 0.])

    # Drag
    f_d = velocity * -(const.BB_DRAG_COEFFICIENT * 0.5 * const.AIR_DENSITY *
                       speed * const.BB_AREA)
    # note that some formulas say speed ** 2, but we already have speed ** 2
    # because we're multiplying speed by velocity
    delta_velocity += f_d / const.BB_MASS

    # Magnus Force
    #f_m = const.BB_DRAG_COEFFICIENT * np.cross(angular_velocity, velocity)
    f_m = const.BB_S_COEFFICIENT * np.cross(angular_velocity, velocity)
    delta_velocity += f_m / const.BB_MASS

    # Finally, update velocity
    velocity += delta_velocity * const.TIME_EPSILON

    # Viscous torque
    torque = (-8. * math.pi * const.BB_RADIUS ** 3 * const.AIR_VISCOSITY *
              angular_velocity)
    angular_velocity += (torque * const.TIME_EPSILON /
                         const.BB_MOMENT_OF_INERTIA)

    return velocity, angular_velocity

if __name__ == '__main__':
    main()
