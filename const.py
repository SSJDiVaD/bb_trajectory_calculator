"""Constants of the BB trajectory calculator"""

import math
import numpy as np

# World params ----------------------------------------------------------------
#AIR_DENSITY = 1.225  # kg / m ** 3  # 15*C at sea level
AIR_DENSITY = 1.1839  # kg / m ** 3  # 25*C at sea level
# Source: https://en.wikipedia.org/wiki/Viscosity#Air
AIR_VISCOSITY = 1.81e-5  # kg / (m * s).
STANDARD_GRAVITY = 9.80665  # standard gravity in m / s ** 2
TIME_EPSILON = 5e-4  # the amount of time in seconds between each step of the
# simulation
FPS_TO_MPS = 0.3048

# Optimization params----------------------------------------------------------
MAX_HEIGHT = 0.3  # If the BB travelled more than this high above the muzzle,
# the trajectory is considered undesirable
MIN_HEIGHT = 0.3  # Simulation stops when bb drops this distance below the
# muzzle in meters
#MAX_DISTANCE = 60
#MAX_DISTANCE = 45.72  # Simulation stops after BB flies this far
#MAX_DISTANCE = 30.48
MAX_DISTANCE = 15.24
# we will try all angular speeds from the first number to the second number
TUNE_ANGULAR_SPEED_RANGE = 30, 100  # 30 is a good lower bound for 415FPS, .25g
TUNE_ANGULAR_SPEED_STRIDE = 0.25  # we will stride by this amount when
# testing different angular speeds
# we will try all muzzle angles from the first number to the second number
TUNE_MUZZLE_ANGLES = 0., 0.001
TUNE_MUZZLE_ANGLE_STRIDE = 0.001

# Gun params ------------------------------------------------------------------
MUZZLE_SPEED_EQUIVALENT_FPS_INIT = 415.  # speed in feet / s of a .2g bb fired
# from the same gun; this will be adjusted for BB mass
MUZZLE_ANGULAR_SPEED = 49  # radians per second backspin
MUZZLE_ANGLE = 0.00  # angle of elevation above horizon in radians

# BB params -------------------------------------------------------------------
BB_MASS_G = 0.36  # mass of bb in g
BB_MASS = BB_MASS_G / 1000  # mass of bb in kg
BB_DIAMETER_MM = 5.95  # diameter of a BB in mm
BB_RADIUS = BB_DIAMETER_MM / 2000.  # radius of a BB in m
BB_AREA = math.pi * BB_RADIUS ** 2.  # cross sectional area of a BB in m ** 2
BB_DRAG_COEFFICIENT = 0.47  # dimensionless drag coefficient of a sphere
BB_MOMENT_OF_INERTIA = (2. / 5.) * BB_MASS * BB_RADIUS ** 2  # moment of
# inertia of a sphere
BB_S_COEFFICIENT = 16. / 3. * math.pi ** 2 * BB_RADIUS ** 3 * AIR_DENSITY

if __name__ == '__main__':
    import calc
    calc.main()
